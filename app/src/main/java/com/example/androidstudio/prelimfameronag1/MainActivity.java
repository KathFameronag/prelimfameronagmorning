package com.example.androidstudio.prelimfameronag1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText input =(EditText)findViewById(R.id.editTextInput);
        final Button btn1 = (Button)findViewById(R.id.btnTry);
        final Button btn2= (Button)findViewById(R.id.btnReveal);
        final TextView txt= (TextView) findViewById(R.id.textView);

        btn1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Random rand = new Random();
                int randomNumber = rand.nextInt(10);

                int guess = Integer.parseInt(input.getText().toString());

                if( guess == randomNumber)

                {
                    Toast.makeText(getApplicationContext(), "Congratulations! You guessed it", Toast.LENGTH_LONG).show();
                }

                if( guess > randomNumber)

                {
                    Toast.makeText(getApplicationContext(), "Lower", Toast.LENGTH_LONG).show();
                }

                else
                {
                    Toast.makeText(getApplicationContext(), "Higher", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Random rand = new Random();
                int randomNumber = rand.nextInt(10);
                    txt.setText(new Double(randomNumber).toString());
            }
        });


    }

}
